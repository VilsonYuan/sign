#!/usr/bin/python
#encoding:utf-8

import httplib
import os.path
import re
import tornado.auth
import tornado.ioloop
import tornado.options
import tornado.web
import unicodedata
import pymongo
import hashlib
import datetime
import time
import tornado.httpserver

from tornado.options import define, options

define('port', default=8888, help='run on the given port', type=int)
conn = pymongo.Connection('localhost', 27017)
db = conn.Work
account = db.Sign  #sign为collection
user = db.addUser  #addUser为Collection

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
                (r'/comments/', CommentsHandler),
                (r'/signout/$', Check_outHandler),
                (r'/detail/$', DetailHandler),
                (r'/', ShowAllHandler),
            ]
        settings = dict(
                template_path=os.path.join(os.path.dirname(__file__), 
                    "templates"),
                static_path=os.path.join(os.path.dirname(__file__), "static"),
                debug=True,
            )
        tornado.web.Application.__init__(self, handlers, **settings)

class DetailHandler(tornado.web.RequestHandler):
    def get(self):
        flag = []
        nowtime = get_time()
        year = nowtime[:4]
        month = nowtime[5:7]
        day = nowtime[8:10]
        username = self.get_argument('username')
        if username:
            message = account.find({'username':username, 'year':year, \
                    'month':month, 'day':day})
            for i in message:
                flag.append(i)
            if flag:
                errorinfo = "今日已签到,无需重复签到!!!"
            else:
                errorinfo = ""
            self.render('showone.html', message=account.find({'username':username}), 
                    username=username, errorinfo=errorinfo)
        else:
            self.redirect("/")
    def post(self):
        flag = []
        nowtime = get_time()
        #year = time.strftime("%Y")
        year = nowtime[:4]
        #month = time.strftime('%m')
        month = nowtime[5:7]
        #day = time.strftime('%d')
        day = nowtime[8:10]
        starttime = datetime.datetime.strptime(nowtime, "%Y-%m-%d %H:%M:%S")
        username = self.get_argument('username')
        for i in account.find({'username':username, 'year':year, 'month':month, 'day':day}):
            flag.append(i)
        if flag:
            account.update({'starttime':starttime},{'$set':flag[0]})
        else:
            account.insert({'username':username, 'year':year, 'month':month, 'day':day, 'starttime':starttime})
        self.get()

class ShowAllHandler(tornado.web.RequestHandler):
    def get(self):
        message = []
        allyear = []
        count = 0
        try:
            year = self.get_argument('year')
            month = self.get_argument('month')
        except:
            nowtime = get_time()
            year = nowtime[:4]
            month = nowtime[5:7]
            
        for i in user.find():
            for t in account.find({"username":i['username'], "year":year, "month":month}):
                if t.get('starttime') and t.get('endtime'):
                    if (t['starttime']-t['starttime']).seconds > 32400:   #每天的工作时间大于9小时
                        count += 1       #签到天数
            lunch_m = count * 15    #午餐补助
            overtime = 0    #加班天数
            if count:
                for j in account.find({"username":i["username"], "year":year, "month":month}):
                    if j.get('endtime'):
                        if j["endtime"].strftime("%H:%M") > "19:00":
                            overtime += 1
                user1 = [i["username"], count, lunch_m, overtime, 'image']
            else:
                user1 = [i["username"], 0, 0, 0, i['image']]
            message.append(user1)
        for i in account.find():
            if i.get("year"):
                allyear.append(i["year"])
        allyear = list(set(allyear))
        allyear.sort()
        print message
        self.render("showall.html", users=message, allyear=allyear, 
                year=year, month=month, users1=user.find())
    def post(self):
        self.redirect('/')

class Check_outHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_argument('username')
        self.render('signout.html', username=username)
    def post(self):
        flag = []
        nowtime = get_time()
        #year = time.strftime("%Y")
        year = nowtime[:4]
        #month = time.strftime('%m')
        month = nowtime[5:7]
        #day = time.strftime('%d')
        day = nowtime[8:10]
        endtime = datetime.datetime.strptime(nowtime, "%Y-%m-%d %H:%M:%S")
        username = self.get_argument('username')
        for i in account.find({'username':username, 'year':year, 'month':month, 'day':day}):
            flag.append(i)
        print flag
        if flag:
            account.update({'username':username, 'year':year, 'month':month, 'day':day}, {"$set":{'endtime':endtime}})
        else:
            account.insert({'username':username, 'year':year, 'month':month, 'day':day, 'endtime':endtime})
        self.redirect('/detail/?username='+username)

class CommentsHandler(tornado.web.RequestHandler):
    def post(self):
        flag = []
        username = self.get_argument('username')
        year = self.get_argument('year')
        month = self.get_argument('month')
        day = self.get_argument('day')
        comments = self.get_argument('comments')
        print type(month)
        print type(day)
        if int(month) < 10:
            month = '0' + month
        if int(day) < 10:
            day = '0' + day
        for i in account.find({'username':username, 'year':year, 'month':month, 'day':day}):
            flag.append(i)
        if flag:
            account.update({'username':username, 'year':year, 'month':month, 'day':day}, {'$set':{'comments':comments}})
        else:
            account.insert({'username':username, 'year':year, 'month':month, 'day':day, 'comments':comments})
        self.redirect('/detail/?username='+username)

def get_time():
    """
    获取网络时间
    """
    conn=httplib.HTTPConnection('www.baidu.com')
    conn.request("GET", "/")
    r=conn.getresponse()
    #r.getheaders() #获取所有的http头
    ts=  r.getheader('date') #获取http头date部分
    #将GMT时间转换成北京时间
    ltime= time.strptime(ts[5:25], "%d %b %Y %H:%M:%S")
    ttime=time.localtime(time.mktime(ltime)+8*60*60)
    dat="date -s %u-%02u-%02u"%(ttime.tm_year,ttime.tm_mon,ttime.tm_mday)
    tm="date -s %02u:%02u:%02u"%(ttime.tm_hour,ttime.tm_min,ttime.tm_sec)
    return dat[-10:] + tm[-9:]

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    main()
            
